import unittest
from selenium import webdriver
import page
import config
import logging


class PythonOrgSearch(unittest.TestCase):
    SITE_URL = 'http://todomvc.com/examples/angularjs/#/'

    def setUp(self):
        try:
            self.driver = webdriver.Chrome(config.WEB_DRIVER_PATH)
            self.driver.get(self.SITE_URL)
        except Exception as e:
            logging.exception(e)

            raise

    def test_add_task_success(self):
        main_page = page.MainPage(self.driver)
        main_page.add_task("toDo1")
        assert main_page.check_task_by_id("toDo1", 0), "task in not added"

    def test_add_tasks_success(self):
        main_page = page.MainPage(self.driver)
        tasks = ["toDo1", "toDo2", "toDo3", "toDo4", "toDo5", "toDo6", "toDo7"]
        main_page.add_tasks(tasks)
        assert main_page.check_all_tasks_by_id(tasks), "task in not added"

    def test_delete_task_success(self):
        main_page = page.MainPage(self.driver)
        tasks = ["toDo0", "toDo1", "toDo2", "toDo3", "toDo4", "toDo5", "toDo6", "toDo7"]
        main_page.add_tasks(tasks)
        num_of_tasks = main_page.num_of_tasks()
        main_page.delete_task(1)
        num_of_tasks_changed = main_page.num_of_tasks()

        assert not main_page.check_task_by_id("toDo1", 1), "task is not deleted not added"
        assert num_of_tasks - num_of_tasks_changed == 1, "task is not deleted not added"

    def test_check_out_task_success(self):
        main_page = page.MainPage(self.driver)
        tasks = ["toDo0", "toDo1", "toDo2", "toDo3", "toDo4", "toDo5", "toDo6", "toDo7"]
        main_page.add_tasks(tasks)

        main_page.check_task(2)
        main_page.check_task(3)
        main_page.check_task(4)
        i = 2

        comp_tasks = main_page.get_completed_tasks()
        for task in comp_tasks:
            assert task.text == tasks[i], "task is not checked"
            i += 1

        i = 0
        uncomp_tasks = main_page.get_uncompleted_tasks()
        for task in uncomp_tasks:
            assert task.text == tasks[i], "task is checked, but don't has to be checked"
            if i == 1:
                i += 3
            i += 1

    def test_check_active(self):
        main_page = page.MainPage(self.driver)
        tasks = ["toDo0", "toDo1", "toDo2", "toDo3", "toDo4", "toDo5", "toDo6", "toDo7"]
        main_page.add_tasks(tasks)

        main_page.check_task(4)
        main_page.check_task(5)
        main_page.check_task(6)

        uncomp_tasks = main_page.get_uncompleted_tasks()
        uncomp_tasks_text = main_page.tasks_to_text(uncomp_tasks)
        main_page.see_active_tasks()
        active_tasks = main_page.read_tasks()
        active_tasks_text = main_page.tasks_to_text(active_tasks)
        assert uncomp_tasks_text == active_tasks_text, "active button does not work"

    def test_check_completed(self):
        main_page = page.MainPage(self.driver)
        tasks = ["toDo0", "toDo1", "toDo2", "toDo3", "toDo4", "toDo5", "toDo6", "toDo7"]
        main_page.add_tasks(tasks)

        comp_tasks = main_page.get_completed_tasks()
        comp_tasks_text = main_page.tasks_to_text(comp_tasks)
        main_page.see_completed_tasks()
        c_tasks = main_page.read_tasks()
        c_tasks_text = main_page.tasks_to_text(c_tasks)
        assert comp_tasks_text == c_tasks_text, "completed button does not work"

    def test_check_all(self):
        main_page = page.MainPage(self.driver)
        tasks = ["toDo0", "toDo1", "toDo2", "toDo3", "toDo4", "toDo5", "toDo6", "toDo7"]
        main_page.add_tasks(tasks)

        main_page.see_completed_tasks()
        main_page.see_all_tasks()
        all_tasks = main_page.read_tasks()
        all_tasks_text = main_page.tasks_to_text(all_tasks)
        assert all_tasks_text == tasks, "all button does not work"

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
