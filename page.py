from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage(object):
    def __init__(self, dr):
        self.driver = dr

    def _find_edit_text_bar(self, locator):
        elem = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locator)
        )
        return elem

    edit_text_locator = (By.CLASS_NAME, 'new-todo')

    def add_tasks(self, tasks_to_load):
        """Adding tasks"""
        for task in tasks_to_load:
            self.add_task(task)

    def add_task(self, task):
        edit_text = self._find_edit_text_bar(self.edit_text_locator)
        edit_text.send_keys(task)
        edit_text.send_keys(Keys.ENTER)

    def read_tasks(self):
        return self.driver.find_elements_by_css_selector("li .view")

    def read_task(self, task_id):
        xpath_selector = "//li["+str(task_id+1)+"]/div/label"
        return self.driver.find_element_by_xpath(xpath_selector)

    def num_of_tasks(self):
        all_tasks = self.read_tasks()
        return len(all_tasks)

    def delete_task(self, task_id):
        xpath_target_task = "//li[" + str(task_id + 1) + "]/div"

        target_task = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, xpath_target_task))
        )
        ActionChains(self.driver).move_to_element(target_task).perform()

        xpath_del_button_selector = xpath_target_task + "/button"
        del_button = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, xpath_del_button_selector))
        )
        del_button.click()

    def check_task(self, task_id):
        xpath_selector = "(//input[@type='checkbox'])[" + str(task_id+2) + "]"
        check_button = self.driver.find_element_by_xpath(xpath_selector)
        self.driver.execute_script("arguments[0].click();", check_button)

    def get_completed_tasks(self):
        return self.driver.find_elements_by_css_selector("li.completed .view")

    def get_uncompleted_tasks(self):
        return self.driver.find_elements_by_css_selector("li:not(.completed) .view")

    def see_all_tasks(self):
        all_button = self.driver.find_element_by_xpath("//footer/ul/li/a")
        self.driver.execute_script("arguments[0].click();", all_button)

    def see_active_tasks(self):
        all_button = self.driver.find_element_by_xpath("//footer/ul/li[2]/a")
        self.driver.execute_script("arguments[0].click();", all_button)

    def see_completed_tasks(self):
        all_button = self.driver.find_element_by_xpath("//footer/ul/li[3]/a")
        self.driver.execute_script("arguments[0].click();", all_button)


class MainPage(BasePage):
    def task_exist(self, task):
        all_tasks = self.read_tasks()
        text_tasks = ["somth"]
        for single_task in all_tasks:
            text_tasks.append(single_task.text)
        text_tasks.append("some text")
        text_tasks.append("some other text")
        return task in text_tasks

    def check_task_by_id(self, task_to_check, task_id):
        fnd_task = self.read_task(task_id)
        text_tasks = fnd_task.text
        return text_tasks == task_to_check

    def check_all_tasks_by_id(self, tasks_to_check):
        i = 0
        for task in tasks_to_check:
            if not self.check_task_by_id(task, i):
                return False
            i += 1
        return True

    def tasks_to_text(self, tasks):
        text_tasks = ["somth"]
        for single_task in tasks:
            text_tasks.append(single_task.text)
        text_tasks.pop(0)
        return text_tasks
